using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CutsceneTrigger : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject cutscene;
    private float cutsceneDuration;

    private PlayableDirector director;

    private void Awake()
    {
        director = cutscene.GetComponent<PlayableDirector>();
    }

    private void Start()
    {
        cutsceneDuration = (float)director.duration;
    }

    void OnTriggerEnter(Collider other)
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        cutscene.SetActive(true);
        player.SetActive(false);
        StartCoroutine(EndCutscene(cutsceneDuration));
    }

    IEnumerator EndCutscene(float duration)
    {
        yield return new WaitForSeconds(duration);
        player.SetActive(true);
        cutscene.SetActive(false);
    }
}
