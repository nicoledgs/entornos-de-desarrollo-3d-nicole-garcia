using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    [SerializeField] Transform characterRoot;

    public Camera cam;
    private InputData input;
    private CharacterAnimBaseMovement characterMovement;

    public bool onInteractionZone { get; set; }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        characterMovement = GetComponent<CharacterAnimBaseMovement>();
    }
    private void Update()
    {
        input.getInput();
        characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
    }
}
