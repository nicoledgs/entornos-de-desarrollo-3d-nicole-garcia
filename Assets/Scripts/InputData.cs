using UnityEngine;
using System;

[Serializable]

public struct InputData
{
    public float hMovement;
    public float vMovement;

    public float hMouse;
    public float vMouse;

    public bool dash;
    public bool jump;

    public void getInput()
    {
        hMovement = Input.GetAxis("Horizontal");
        vMovement = Input.GetAxis("Vertical");

        vMouse = Input.GetAxis("Mouse Y");
        vMouse = Input.GetAxis("Mouse X");
        dash = Input.GetButton("Dash");
        jump = Input.GetButtonDown("Jump");
    }

}

